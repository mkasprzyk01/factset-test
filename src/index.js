import React from 'react';
import ReactDOM from 'react-dom';
import CurrencyConverterView from './components/currencyConverter/View'

ReactDOM.render(
  <React.StrictMode>
    <CurrencyConverterView />
  </React.StrictMode>,
  document.getElementById('currencyConverter')
);

