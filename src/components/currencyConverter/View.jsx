//React and tools
import React from 'react';
import { useState, useEffect } from 'react';
import Moment from 'react-moment';

//Styles
import './View.css';

//Data
import { currencies } from './currencies.json';
import translation from './translation.json';

function CurrencyConverterView() {
    //Declare States
    const [baseAmount, setBaseAmount] = useState(1);
    const [baseCurrency, setBaseCurrency] = useState('EUR');
    const [destinationAmount, setDestinationAmount] = useState('');
    const [destinationCurrency, setDestinationCurrency] = useState('USD');
    const [converted, setConverted] = useState(false);
    const [message, setMessage] = useState('');
    const [language, setLanguage] = useState('de');
    const [exchangeData, setExchangeData] = useState({});

    const convertFromDestination = (destinationAmount, baseCurrency, destinationCurrency) => {
        if (baseCurrency !== exchangeData.base) {
            //get new exchange rates for new currency conversion
            fetch('https://api.exchangeratesapi.io/latest?base=' + baseCurrency)
                .then(res => res.json())
                .then(data => {
                    setMessage('');
                    setExchangeData(data);

                    setBaseAmount(destinationAmount / data.rates[destinationCurrency]);
                    setBaseCurrency(baseCurrency);
                    setDestinationAmount(destinationAmount);
                    setDestinationCurrency(destinationCurrency);
                    setConverted(true);
                })
                .catch(err => {
                    console.log(err);
                    setMessage(translation.error[language]);
                });
        } else {
            //convert Currency with allready stored exchange rates
            setBaseAmount(destinationAmount / exchangeData.rates[destinationCurrency]);
            setBaseCurrency(baseCurrency);
            setDestinationAmount(destinationAmount);
            setDestinationCurrency(destinationCurrency);
            setConverted(true);
        }
    };

    const convert = (amount, baseCurrency, destinationCurrency) => {
        if (baseCurrency !== exchangeData.base) {
            //get new exchange rates for new currency conversion
            fetch('https://api.exchangeratesapi.io/latest?base=' + baseCurrency)
                .then(res => res.json())
                .then(data => {
                    setMessage('');
                    setExchangeData(data);
                    setBaseAmount(amount);
                    setBaseCurrency(baseCurrency);
                    setDestinationAmount(amount * data.rates[destinationCurrency]);
                    setDestinationCurrency(destinationCurrency);
                    setConverted(true);
                })
                .catch(err => {
                    console.log(err);
                    setMessage(translation.error[language]);
                });
        } else {
            //convert Currency with allready stored exchange rates
            setBaseAmount(amount);
            setBaseCurrency(baseCurrency);
            setDestinationAmount(amount * exchangeData.rates[destinationCurrency]);
            setDestinationCurrency(destinationCurrency);
            setConverted(true);
        }
    };

    //expose conversion method to outside
    window.CurrencyConverter = {
        convert: convert
    };

    useEffect(() => {
        convert(baseAmount, baseCurrency, destinationCurrency);
    });

    return (
        <div className="CurrencyConverter">
            {message && (
                <div className="CurrencyConverter-errormessage">
                    <div className="CurrencyConverter-errormessage__text">{message}</div>
                </div>
            )}

            {converted && (
                <div className="CurrencyConverter-convertedmessage">
                    <div className="CurrencyConverter-convertedmessage__toprow">
                        {baseAmount + ' ' + baseCurrency + ' ' + translation.equals[language]}
                    </div>
                    <div className="CurrencyConverter-convertedmessage__mainrow">
                        {destinationAmount + ' ' + destinationCurrency}
                    </div>
                    <div className="CurrencyConverter-convertedmessage__timestamp">
                        <Moment format="DD.MM.YYYY" parse="YYYY-MM-DD">
                            {exchangeData.date || ''}
                        </Moment>
                    </div>
                </div>
            )}

            <div className="CurrencyConverter-inputgroup">
                <input
                    className="CurrencyConverter-inputgroup__input"
                    type="number"
                    name="baseAmount"
                    value={baseAmount}
                    onChange={e => {
                        convert(e.target.value, baseCurrency, destinationCurrency);
                        setBaseAmount(e.target.value < 0 || e.target.value === '' ? 0 : e.target.value);
                    }}
                />
                <select
                    className="CurrencyConverter-inputgroup__select"
                    name="baseCurrency"
                    value={baseCurrency}
                    onChange={e => {
                        setBaseCurrency(e.target.value);
                        convert(baseAmount, e.target.value, destinationCurrency);
                    }}
                >
                    {currencies.map(currency => (
                        <option key={currency} value={currency}>
                            {currency}
                        </option>
                    ))}
                </select>
            </div>

            <div className="CurrencyConverter-inputgroup">
                <input
                    className="CurrencyConverter-inputgroup__input"
                    type="number"
                    name="destinationAmount"
                    value={destinationAmount}
                    onChange={e => {
                        setDestinationAmount(e.target.value);
                        convertFromDestination(e.target.value, baseCurrency, destinationCurrency);
                    }}
                />
                <select
                    className="CurrencyConverter-inputgroup__select"
                    name="destinationCurrency"
                    value={destinationCurrency}
                    onChange={e => {
                        setDestinationCurrency(e.target.value < 0 || e.target.value === '' ? 0 : e.target.value);
                        convert(baseAmount, baseCurrency, e.target.value);
                    }}
                >
                    {currencies.map(currency => (
                        <option key={currency} value={currency}>
                            {currency}
                        </option>
                    ))}
                </select>
            </div>

            <div className="CurrencyConverter-language">
                <select
                    className="CurrencyConverter-language__select"
                    name="language"
                    value={language}
                    onChange={e => setLanguage(e.target.value)}
                >
                    <option value="en">English</option>
                    <option value="de">Deutsch</option>
                </select>
            </div>
        </div>
    );
}

export default CurrencyConverterView;
